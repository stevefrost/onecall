<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Quote;

class QuoteController extends Controller
{
    public function index() {
        $sales = Quote::whereDate('created_at', \DB::raw('CURDATE()'))->sum('value');
        return view('quote.index')->with('sales', $sales);
    }

    public function create($id) {
        $customer = Customer::find($id);

        $quote = rand(400, 800);

        $from = new \DateTime($customer->date_of_birth);
        $to   = new \DateTime('today');
        if ($from->diff($to)->y > 70) {
            // if the customer is over 70
            $quote += 100;
        }

        if (strtolower($customer->vehicle->make) === 'vauxhall' && strtolower($customer->vehicle->model) === 'agila') {
            // if the customer drives a Vauxhall Agila
            $quote += 100;
        }

        return view('quote/create')->with('customer', $customer)->with('quote', $quote);
    }

    public function store(Request $request) {
        $quote = new Quote();
        $quote->customer_id = $request->customer_id;
        $quote->vehicle_id = $request->vehicle_id;
        $quote->value = $request->quote_value;
        $quote->save();

        return redirect('/sales');
    }
}
