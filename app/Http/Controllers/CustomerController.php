<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Address;
use App\Vehicle;

class CustomerController extends Controller
{

    public function create() {
        return view('customer.create');
    }

    public function store(Request $request) {
        $validations = $this->validate($request, [
            'name' => 'required',
            'date_of_birth' => 'required',
            'licence_number' => 'required',
            'address_line_1' => 'required',
            'town' => 'required',
            'county' => 'required',
            'postcode' => 'required',
            'make' => 'required',
            'model' => 'required',
            'registration_number' => 'required'
        ]);
        $customer = new Customer();
        $customer->saveCustomer($request);

        $address = new Address();
        $address->saveAddress($request);

        $vehicle = new Vehicle();
        $vehicle->saveVehicle($request);

        $customer->address()->save($address);
        $customer->vehicle()->save($vehicle);


        return redirect('/quote/new/'.$customer->id);
    }
}
