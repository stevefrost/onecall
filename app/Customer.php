<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function vehicle() {
        return $this->hasOne('App\Vehicle');
    }

    public function address() {
        return $this->hasOne('App\Address');
    }

    public function saveCustomer($data) {
        $this->name = $data['name'];
        $this->date_of_birth = $data['date_of_birth'];
        $this->driving_licence_number = $data['licence_number'];
        $this->save();

    }
}
