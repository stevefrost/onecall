@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h4>You are creating a quote for {{ $customer->name }}</h4>
        <p class="lead">The customers vehicle is a {{ $customer->vehicle->make }} {{ $customer->vehicle->model }}</p>
        <p class="lead">The quote for insurance is £{{ $quote }}</p>
        <form action="{{ url('/quote/store') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="customer_id" value="{{ $customer->id }}">
            <input type="hidden" name="vehicle_id" value="{{ $customer->vehicle->id }}">
            <input type="hidden" name="quote_value" value="{{ $quote }}">
            <input type="submit" class="btn btn-primary" value="Save Quote">
            <a class="btn btn-secondary" href="{{ url('/') }}">Cancel</a>
        </form>
    </div>

@endsection