<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index.index');
});

Route::get('/customer/create', 'CustomerController@create');
Route::get('/quote/new/{id}', 'QuoteController@create');
Route::get('/sales', 'QuoteController@index');

Route::post('/customer/store', 'CustomerController@store');
Route::post('/quote/store', 'QuoteController@store');
