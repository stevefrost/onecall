<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public function customer() {
        return $this->belongsTo('App\Customer');
    }

    public function saveVehicle($data) {
        $this->make = $data['make'];
        $this->model = $data['model'];
        $this->registration_number = $data['registration_number'];
        $this->save();
    }
}
