<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function customer() {
        return $this->belongsTo('App\Customer');
    }

    public function saveAddress($data) {
        $this->address_line_1 = $data['address_line_1'];
        $this->address_line_2 = $data['address_line_2'];
        $this->town = $data['town'];
        $this->county = $data['county'];
        $this->postcode = $data['postcode'];
        $this->save();
    }
}
