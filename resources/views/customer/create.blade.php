@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ url('/customer/store') }}" class="form-horizontal" method="post">
        {{ csrf_field() }}
    <div id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <div data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Personal Details
                    </div>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <fieldset>
                        <div class="form-group">
                            <label for="" class="">Full Name</label>
                            <input type="text" name="name" class="form-control" placeholder="John Smith">
                        </div>
                        <div class="form-group">
                            <label for="" class="">Date of Birth</label>
                            <input type="date" name="date_of_birth" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="">Driving Licence Number</label>
                            <input type="text" name="licence_number" class="form-control">
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <div class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Address
                    </div>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <fieldset>
                        <div class="form-group">
                            <label for="">Address Line 1</label>
                            <input type="text" name="address_line_1" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Address Line 2</label>
                            <input type="text" name="address_line_2" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Town</label>
                            <input type="text" name="town" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">County</label>
                            <input type="text" name="county" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Postcode</label>
                            <input type="text" name="postcode" class="form-control">
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                    <div class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Vehicle
                    </div>
                </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <fieldset>
                        <div class="form-group">
                            <label for="">Make</label>
                            <input type="text" name="make" class="form-control" placeholder="Vauxhall">
                        </div>
                        <div class="form-group">
                            <label for="">Model</label>
                            <input type="text" name="model" class="form-control" placeholder="Agila">
                        </div>
                        <div class="form-group">
                            <label for="">Registration Number</label>
                            <input type="text" name="registration_number" class="form-control" placeholder="FR05 TY1">
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Save and Create Quote">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
@endsection